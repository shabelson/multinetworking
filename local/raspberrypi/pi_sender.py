from Adafruit_SHT31 import *
import serial
import time

# Define SHT31 Address
sensor = SHT31(address = 0x44)

ser = serial.Serial(
        port='/dev/ttyS0',
        baudrate = 9600,
        parity=serial.PARITY_NONE,
        stopbits=serial.STOPBITS_ONE,
        bytesize=serial.EIGHTBITS,
        timeout=1
)

def readSHT():
        try:
                return sensor.read_temperature(), sensor.read_humidity()
        except:
                return None, None


if __name__ == "__main__":
        # import argparse
        # parser = argparse.ArgumentParser()

        # parser.add_argument("--verbose", "-v", dest='verbose', action='store_true', $
        # parser.set_defaults(keep=False)
        # args = parser.parse_args()
        try:
                while True:
                        temp, humidity = readSHT()
                        time.sleep(1)
        #               if args.verbose:
                        print 'Temp             = {0:0.3f} deg C'.format(temp)
                        print 'Humidity         = {0:0.2f} %'.format(humidity)
                        try:
                                ser.write('T:')
                                ser.write(str(temp))
                                ser.write(',')
                                ser.write('H:')
                                ser.write(str(humidity))
                                ser.write('\n')
                        except:
                                print ('Serial problem')
                                pass
        except KeyboardInterrupt:
                raise SystemExit