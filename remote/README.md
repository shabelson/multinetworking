# Remote networks

![](https://www.survivingwithandroid.com/wp-content/uploads/2016/10/mqtt_publisher_subscriber-1.png)

## Clients

- [Arduino PubSubClient library](https://pubsubclient.knolleary.net/api.html)
- [Python paho-MQTT](https://pypi.org/project/paho-mqtt/)
- [Grasshopper](https://github.com/256dpi/grasshopper-mqtt)

## Using with a raspberry-pi and node-red

Set up a raspberry pi using the following instructions [from here](https://gist.github.com/xoseperez/e23334910fb45b0424b35c422760cb87#file-rpi3_iot_server-md) (at least the following):

- Configure access
- Configure MQTT
- Configure node-red

Then, install [node-red-dashboard](https://www.npmjs.com/package/node-red-dashboard) for the data visualisation and configure the flows at will.

![](inaction.png)

![](https://i.imgur.com/u28hd55.png)

### Configuration

Can set users in mqtt server by following instructions [from here](http://www.steves-internet-guide.com/mqtt-username-password-example/), and [here](https://learn.adafruit.com/diy-esp8266-home-security-with-lua-and-mqtt/configuring-mqtt-on-the-raspberry-pi).

If you are using a remote raspberry-pi, you can get a DNS hostname by using a temporary provider like [noip](https://www.noip.com). Then, follow at least [these instructions to secure it](https://www.raspberrypi.org/documentation/configuration/security.md) and in [nodered](https://github.com/node-red/cookbook.nodered.org/wiki/How-to-safely-expose-Node-RED-to-the-Internet). Get the default config file and create a hash password with:

```
node-red-admin hash-pw
```

Then put it in `.node-red/settings.js`, using the default from [here](https://github.com/node-red/node-red/blob/master/packages/node_modules/node-red/settings.js). Good ssl certificate [here](https://github.com/FiloSottile/mkcert). You will need to open the ports 80, 443, 1880 and 1883 on the pi to the world. Consider a firewall or similar for extra cautions or an nginx proxy.

### Useful debugging commands

- MQTT:

```
# Check status of mosquitto server
sudo service mosquitto status
# Restart mosquitto
sudo systemctl restart mosquitto
# Subscribe to a topic
mosquitto_sub -p 1883 -u USER -P PASSWORD -h HOST -t TOPIC
# Or
mosquitto_sub -p 1883 -u USER -P PASSWORD -h HOST -t TOPIC/#
# Or
mosquitto_sub -p 1883 -u USER -P PASSWORD -h HOST -t TOPIC/+/inputs
```